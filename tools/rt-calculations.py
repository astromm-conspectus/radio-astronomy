#!/usr/bin/env python3
import argparse
import numpy as np
from scipy.optimize import least_squares


def curve_length(x: float, p: float) -> float:
    """
    Returns length of parabolic curve.
    """
    sum1 = 2 * x * np.sqrt(4 + 2 * p / x)
    sum2 = (p + 4 * x + sum1) / p
    return (sum1 + p * np.log(sum2)) / 4


def get_solutions(length_array, p: float):
    """
    Find grid of coordinates of circles at a same distance.
    """
    solutions, errors = [], []
    for length in length_array:
        sol = least_squares(lambda x: curve_length(x, p) - length, x0=0, bounds=(0, 100))
        solutions.append(sol.x[0])
        errors.append((curve_length(sol.x, p) - length)[0])
    return solutions, errors


def main(args):
    length_array = np.arange(args.array[0], args.array[1], args.array[2])
    solutions, errors = get_solutions(length_array, args.p)
    solutions_str_list = ["%0.5f" % item for item in solutions]
    solutions_str = ', '.join(solutions_str_list)

    rel_err = []
    for i in range(len(errors)):
        rel_err.append(errors[i] / solutions[i])

    print(f"p: {args.p}")
    print(f"Length array: {length_array}")
    print(f"Solutions: {solutions_str}")
    print(f"Relative error: {rel_err}")


# default: p = 20, array: 0.4 7.4 0.8
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Python script for calculating a grid of coordinates of circles at a same distance.")
    parser.add_argument("p", action='store', type=float, help="input parameter of parabola")
    parser.add_argument("array", nargs=3, action='store', type=float,
                        help="input length array: start final step")
    args = parser.parse_args()
    main(args)

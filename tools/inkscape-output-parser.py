#!/usr/bin/env python3
import argparse


def parse_line(line: str) -> dict:
    """
    Extract line's parameters (start x, delta x and y) from text line.
    """
    point1, point2 = tuple(line.split('--'))
    point1 = point1.split('(', 1)[1].split(')')[0]
    point2 = point2.split('(', 1)[1].split(')')[0]
    x1, y1 = map(float, tuple(point1.split(',')))
    x2, y2 = map(float, tuple(point2.split(',')))
    y = (y1 + y2) / 2
    dx = x2 - x1
    return {'x': x1, 'dx': dx, 'y': y}


def parse_output(text: str) -> dict:
    """
    Parse raw output of draw/path commands.
    Awaited format:
    ```
    ...
    \draw[impLine] (14.0031,12.6410) -- (15.7538,12.6586);
    \draw[impLine] (13.2414,12.8810) -- (15.1853,12.8766);
    ...
    ```
    """
    lines_list = []
    for line in text.split('\n'):
        lines_list.append(parse_line(line))
    lines_list = sorted(lines_list, key=lambda d: d['y'])
    #points = {key: [dic[key] for dic in lines_list] for key in lines_list[0]}
    return lines_list


def print_new_output(filename: str, points):
    """
    Write parsed points into the given file (by filename).
    """
    with open(filename, 'w') as f:
        for point in points:
            f.write(
                f"        \draw[impLine] ({point['x']:.4f},{point['y']:.4f}) --++ ({point['dx']:.4f},0);\n")


def read_data(filename: str):
    """
    Read Inkscape tikz output from the given file (by filename).
    """
    with open(filename, 'r') as f:
        text = f.read()
    return text


def main(args):
    text = read_data(args.input_filename,)
    points = parse_output(text[:-1])
    print_new_output(args.output_filename, points)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Python script for parsing Inkscape Tikz output.")
    parser.add_argument('input_filename', action='store', type=str, help="input output file name")
    parser.add_argument('output_filename', action='store', type=str, help="input output file name")
    args = parser.parse_args()
    main(args)
